# README #

Quickly obtain Googdata temporary token.

### Purpose ###

* Temporary token is needed for further API calls
* Source information: https://developer.gooddata.com/api#/introduction/use-cases/log-in

### OSX set-up ###
Install pip:

    sudo easy_install pip

Install libraries:

    sudo pip install requests

### Set-up ###

* Insert your login and password in *config.py*

### Future development ###

* Exceptions
* Command line ARGs interface to run it from terminal

### Contact ###

* fisa(at)keboola.com
* @VFisa