import requests
#import pprint
import json
from config import *


def get_token(username, password):
    """Get token by requesting login spiel and then token spiel"""
    
    values = {
        "postUserLogin":{
            "login":username,
            "password":password,
            "remember":0,
            "verify_level":0
            }
        }

    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
        }

    data = json.dumps(values)
    r = requests.post('https://secure.gooddata.com/gdc/account/login', data=data, headers=headers)
    print ("Login response code: "+str(r.status_code))
    #json_decode=json.loads(r.text)
    
    headers = {'Accept': 'application/json',
	'Content-Type': 'application/json'}
    
    returned = requests.get('https://secure.gooddata.com/gdc/account/token', headers=headers, cookies=r.cookies)
    
    print ("Token response code: "+str(returned.status_code))
    #pprint.pprint(returned.headers)
    ttoken = returned.cookies['GDCAuthTT']
    return ttoken


token = get_token(USERNAME, PASSWORD)
print "Token is:"
print token

